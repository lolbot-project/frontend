/* eslint-env browser */
var xhr = new XMLHttpRequest()
var guilds = document.getElementById('js-bot-guilds')
var online = document.getElementById('js-bot-online')

var base = 'https://api-prod.squiggle.cf/api/'

xhr.onreadystatechange = function () {
  if (this.readyState === 4 && this.status === 200) {
    var response = JSON.parse(this.responseText)
    console.log(response)
    guilds.innerHTML = response.guilds
    if (response.ready === true) {
      online.innerHTML = '<i class="fas fa-check-circle facolors-online"></i> online'
    } else {
      online.innerHTML = '<i class="fas fa-times-circle facolors-offline"></i> not ready'
    }
  }
}
function getInfo () { // eslint-disable-line no-unused-vars
  xhr.open('GET', base + 'stats/basic', true)
  xhr.send()
}

function getVotes () { // eslint-disable-line no-unused-vars
  var db = document.getElementById('js-dbl-votes')
  var db2 = document.getElementById('js-dbl-allvotes')
  xhr.open('GET', base + 'votes', true)
  xhr.send()
  xhr.onreadystatechange = function () {
    if (this.readyState === 4 && this.status === 200) {
      var res = JSON.parse(this.responseText)
      console.log(`[votes:ok] ${res}`)
      db.innerHTML = res.dbl_monthly
      db2.innerHTML = res.dbl_total
    }
  }
}
