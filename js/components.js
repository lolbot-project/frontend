/* eslint-env browser */

/* A hacky solution to Font Awesome not appearing in Shadow DOMs */
function importFA (shadow) {
  var fa = document.createElement('style')
  fa.innerHTML = '@import "https://use.fontawesome.com/releases/v5.6.3/css/all.css"'
  shadow.appendChild(fa)
}

function importCommon (shadow) {
  var common = document.createElement('style')
  common.innerHTML = '@import "/css/common.css"'
  shadow.appendChild(common)
}

function importTailwind (shadow) {
  var tailwind = document.createElement('style')
  tailwind.innerHTML = '@import "https://cdn.jsdelivr.net/npm/tailwindcss/dist/tailwind.min.css"'
  shadow.appendChild(tailwind)
}

/* Messy, but works good enough. */
class Footer extends HTMLElement {
  constructor () {
    super()
    var shadow = this.attachShadow({ mode: 'open' })
    importCommon(shadow)
    importFA(shadow)

    var foot = document.createElement('footer')
    foot.innerHTML = '<p style="text-align: center;">made with &hearts; in texas</p>'

    shadow.appendChild(foot)
  }
}
/* Really messy. */
class TopBar extends HTMLElement {
  constructor () {
    super()
    var shadow = this.attachShadow({ mode: 'open' })
    importTailwind(shadow)
    importCommon(shadow)

    var top = document.createElement('nav')
    top.setAttribute('class', 'flex items-center justify-between flex-wrap menu-bg p-6')

    var titleContainer = document.createElement('div')
    titleContainer.setAttribute('class', 'flex items-center flex-no-shrink text-white mr-6')

    var title = document.createElement('span')
    title.setAttribute('class', 'font-semibold text-xl tracking-tight')
    title.innerHTML = 'lolbot'
    titleContainer.appendChild(title)
    top.appendChild(titleContainer)

    var menu = document.createElement('div')
    menu.setAttribute('class', 'w-full block flex-grow lg:flex lg:items-center lg:w-auto')
    top.appendChild(menu)

    var current = window.location.href

    var links = document.createElement('div')
    menu.appendChild(links)
    links.setAttribute('class', 'text-sm lg:flex-grow')
    var linkClasses = 'block mt-4 lg:inline-block lg:mt-0 text-white hover:text-blue-lightest mr-4'

    var home = document.createElement('a')
    var upvote = document.createElement('a')
    var support = document.createElement('a')
    links.appendChild(home)
    links.appendChild(upvote)
    links.appendChild(support)
    home.setAttribute('class', linkClasses)
    upvote.setAttribute('class', linkClasses)
    support.setAttribute('class', linkClasses)
    home.setAttribute('href', '/')
    support.setAttribute('href', '/server')
    upvote.setAttribute('href', '/upvote')
    home.innerHTML = 'Home'
    support.innerHTML = 'Support'
    upvote.innerHTML = 'Upvote'

    if (current.endsWith('upvote/')) {
      upvote.setAttribute('class', linkClasses + ' font-bold')
      upvote.setAttribute('href', '#')
    } else if (current.endsWith('server/')) {
      support.setAttribute('class', linkClasses + ' font-bold')
      support.setAttribute('href', '#')
    } else if (current.endsWith('/')) {
      home.setAttribute('class', linkClasses + ' font-bold')
      home.setAttribute('href', '#')
    }

    var invite = document.createElement('a')
    invite.setAttribute('class', 'inline-block text-sm px-4 py-2 leading-none border rounded text-white border-white hover:border-transparent hover:text-teal hover:bg-white mt-4 lg:mt-0')
    invite.href = '/invite'
    invite.innerHTML = 'Invite'
    menu.appendChild(invite)

    shadow.appendChild(top)
  }
}

customElements.define('lb-navbar', TopBar)
customElements.define('lb-footer', Footer)
